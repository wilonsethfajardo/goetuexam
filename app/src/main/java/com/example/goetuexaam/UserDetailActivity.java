package com.example.goetuexaam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class UserDetailActivity extends AppCompatActivity {

    TextView userName, userId, userEmail, userAge, userAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUi();

        Intent intent = getIntent();
        userName.setText("Name: "+intent.getStringExtra("name"));
        userId.setText("ID: "+intent.getStringExtra("id"));
        userEmail.setText("Email: "+intent.getStringExtra("email"));
        userAge.setText("Age: "+intent.getStringExtra("age"));
        userAddress.setText("Address: "+intent.getStringExtra("address"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                this.finish();
        }
        return true;
    }

    public void setUi(){
        userAddress = findViewById(R.id.user_address);
        userAge = findViewById(R.id.user_age);
        userEmail = findViewById(R.id.user_email);
        userId = findViewById(R.id.user_id);
        userName = findViewById(R.id.user_name);
    }
}
