package com.example.goetuexaam.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.goetuexaam.Pojo.Product;
import com.example.goetuexaam.Pojo.ProductInformation;
import com.example.goetuexaam.ProductDetailActivity;
import com.example.goetuexaam.R;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private List<ProductInformation> productData;
    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, id;
        RelativeLayout card;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.list_title);
            id = view.findViewById(R.id.list_subtitle);
            card = view.findViewById(R.id.card);
        }
    }


    public ProductAdapter(Product product, Context context) {
        this.productData = product.getProduct();
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.name.setText(productData.get(position).getName());
        holder.id.setText("Product ID: " +productData.get(position).getId());
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra("name", productData.get(position).getName());
                intent.putExtra("desc", productData.get(position).getDescription());
                intent.putExtra("id", productData.get(position).getId().toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productData.size();
    }
}