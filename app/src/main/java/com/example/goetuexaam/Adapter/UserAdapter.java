package com.example.goetuexaam.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.goetuexaam.UserDetailActivity;
import com.example.goetuexaam.Pojo.Information;
import com.example.goetuexaam.Pojo.User;
import com.example.goetuexaam.R;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private List<User> userData;
    private Information data;
    Context activityContext;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, id;
        RelativeLayout card;


        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.list_title);
            id = view.findViewById(R.id.list_subtitle);
            card = view.findViewById(R.id.card);
        }
    }


    public UserAdapter(Information user, Context context) {
        this.data = user;
        userData =  data.getUser();
        this.activityContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.name.setText(userData.get(position).getData().getUserInformation().getFirstname() +" "+ userData.get(position).getData().getUserInformation().getLastname());
        holder.id.setText("User ID: " +userData.get(position).getData().getUserInformation().getId());
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activityContext, UserDetailActivity.class);
                intent.putExtra("address" , userData.get(position).getData().getAddress().getAddress1() + "\n"
                        + userData.get(position).getData().getAddress().getAddress2());
                intent.putExtra("email", userData.get(position).getData().getLogin().getEmail());
                intent.putExtra("name", userData.get(position).getData().getUserInformation().getFirstname() + " "
                + userData.get(position).getData().getUserInformation().getLastname());
                intent.putExtra("age", userData.get(position).getData().getUserInformation().getAge());
                intent.putExtra("id", userData.get(position).getData().getUserInformation().getId().toString());
                activityContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return userData.size();
    }
}