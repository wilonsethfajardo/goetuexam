package com.example.goetuexaam;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.goetuexaam.Fragment.ProductFragment;
import com.example.goetuexaam.Fragment.UserFragment;

public class MainActivity extends AppCompatActivity {

    public FragmentManager manager = getSupportFragmentManager();
    public FragmentTransaction transaction = manager.beginTransaction();
    UserFragment userFragment;
    ProductFragment productFragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_users:
                    userFragment = new UserFragment();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.container, userFragment);
                    transaction.addToBackStack("user");
                    transaction.commit();
                    return true;
                case R.id.navigation_products:
                    productFragment = new ProductFragment();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.container, productFragment);
                    transaction.addToBackStack("product");
                    transaction.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        userFragment = new UserFragment();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.container, userFragment);
        transaction.addToBackStack("user");
        transaction.commit();
    }

}
