package com.example.goetuexaam.Pojo;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("product")
    @Expose
    private List<ProductInformation> product = null;

    public List<ProductInformation> getProduct() {
        return product;
    }

    public void setProduct(List<ProductInformation> product) {
        this.product = product;
    }

}