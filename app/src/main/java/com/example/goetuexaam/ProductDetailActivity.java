package com.example.goetuexaam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class ProductDetailActivity extends AppCompatActivity {

    TextView prodcutName, productId, productDesc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUi();

        Intent intent = getIntent();
        prodcutName.setText("Name: "+intent.getStringExtra("name"));
        productId.setText("ID: "+intent.getStringExtra("id"));
        productDesc.setText("Description: "+intent.getStringExtra("desc"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                this.finish();
        }
        return true;
    }

    public void setUi(){
        productDesc = findViewById(R.id.user_email);
        productId = findViewById(R.id.user_id);
        prodcutName = findViewById(R.id.user_name);
    }
}
